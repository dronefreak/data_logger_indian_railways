#include <91x_lib.h>
#include <absacc.h>
#include "DLEV2_FW_MCB_LCD.h"
#include "DLEV2_FW_MCB_NAND.h"
#include "DLEV2_FW_MCB_Define.h"
#include "DLEV2_FW_MCB_Constants.h"
#include "DLEV2_FW_MCB_UIAPI.h"
# define su8	static unsigned char
# define su16	static unsigned short//int
# define su32	static unsigned long



Data Nand_Start_Ptr;//__at (0x4000000);
Data PrnPort_Read_Ptr;//__at (0x4000010); 
Data PrnSearch_begin_Ptr;//__at (0x4000020);
Data PrnSearch_end_Ptr;//__at (0x4000030);


Data alarm_ptr;
Data disp_alarm_ptr;

u8 write_operation_flag = 0;
//u8 end_record_found_flag = 0;
//u8 start_record_found_flag = 0;
//u8 dle_end_record_found_flag = 0xFF;
u8 dle_start_record_found_flag = 0xFF;
u32 temp_UI_NWPort1_Read_Ptr,temp_UI_NWPort2_Read_Ptr,temp_UI_LMU_Read_Ptr,temp_UI_Write_Ptr;//added by sowdith


void NF_Cmd(void);	   //Command input bus operations 
void NF_Add(void);	   //Address input bus operations
void NF_Write(void);		 //Data input bus operations 
void NF_Read(void);		 //Data output bus operations
void Check_Badblock(void);	 //Identifying number of initial badblocks
u16 Select_Cmd(u16 Add);
u8 fBlock_Erase(u16 NBlock);
void Delay_325ns(u32 nCount);
void Nand_Address(u8 NByte,u8 NPage,u16 NBlock);
extern  void Delay_325ns(u32 nCount); 
void Memory_Write(Segment_Addr* Seg_Addr, u32 offset, u8* data_ptr, u16 no_of_bytes);
void Nand_Read_Records(Segment_Addr* Seg_Addr,u32 offset,u16 No_Of_Bytes);
void Nand_Read_Records_Test(Segment_Addr* Seg_Addr,u32 offset,u16 No_Of_Bytes);
void Nand_Write_Records(u16 size);
u8 Verify_Badpage(u16 Block,u8 Page);
u16 Verify_BadBlock(u16 Block);
u32 Wrt_Rec_No=0,Read_Rec_No=0;
u16 Config_Blocks[NO_OF_CONFIG_BLOCKS];// at(0x84000);
u8 var,loop_var1;//,Pageno,Recordno;
//u8 Recordno,Pageno; 
//u8 Nand_LMU_End_Flag,Nand_NWP1_End_Flag,Nand_NWP2_End_Flag;
//u16 Blockno;
u8 Event_Block_Start;  
u8 Tx_Data_Buffer[256],k,Arr[4];
extern u8 Nand_Buffer[512];
extern u16 Temp_NWPort1_Read_Ptr_Blockno; 
extern u8  Temp_NWPort1_Read_Ptr_Page;    
extern u8  Temp_NWPort1_Read_Ptr_Record;  
extern u32 Temp_UI_NWPort1_Read_Ptr; 

extern u16 Temp_NWPort2_Read_Ptr_Blockno; 
extern u8  Temp_NWPort2_Read_Ptr_Page;    
extern u8  Temp_NWPort2_Read_Ptr_Record;  
extern u32 Temp_UI_NWPort2_Read_Ptr; 

u16 wr_blockno;
u8  wr_pageno,wr_recordno; 

void DLEV2_FW_MCB_Write_dle_records_only(u16 no_of_wr_bytes);

Data dle_storage_start_ptr;
//initiation of search blocks;
Data DLEV2_FW_MCB_Goto_previous_record(u16 temp_block_no, s8 temp_page_no, u8 temp_record_no);
u16 DLEV2_FW_MCB_Get_backward_search_block_no(u32 temp_seq_no, u32 search_seq_no, u16 temp_block_no);
u16 DLEV2_FW_MCB_Get_forward_search_block_no(u32 temp_seq_no, u32 search_seq_no, u16 temp_block_no);
u8 set_time_write_operation_flag = FALSE;

void Nand_GPIO_Init(void)//port initialization
{
	SCU->PCGR1   |= 0x180000;			//GPIO5,6, enabled
	SCU->PRR1	 |= 0x180000;			//GPIO5,6,reset
	SCU->GPIOIN[6]=0x0000;			// P0  Alternate input1 mode disabled  
    SCU->GPIOTYPE[6]=0x0000;			// push pull 
    SCU->GPIOOUT[6]=0x5555;			// P0 is output mode1 genaral purpose out put
    GPIO6->DDR=0xFF;	   		 		// all pins in P0 are o/p
    SCU->GPIOIN[5]=0x0000;			// P5  Alternate input1 mode disabled  
    SCU->GPIOTYPE[5]=0x0000;			// push pull 
    SCU->GPIOOUT[5]=0x0555;			// P5_7, P5_6 are inputs,P[5:0]areoutput mode1 genaral purpose out put
    GPIO5->DDR=0x3F;	  
}
void NF_Cmd(void)	   //Command input bus operations
{
    NAND_CMDS= 0x0A;
//    Delay_325ns(2);
//  	for(k=0;k<1;k++);
	NAND_CMDS= 0x22;
}
void NF_Add(void)	   //Address input bus operations
{
    NAND_CMDS= 0x12;
//	Delay_325ns(2);
//	for(k=0;k<1;k++);
	NAND_CMDS= 0x22;
}

void NF_Write(void)		 //Data input bus operations
{
    NAND_CMDS= 0x02;
//	Delay_325ns(2);
//	for(k=0;k<1;k++);
	NAND_CMDS= 0x22; 
}
void NF_Read(void)		 //Data output bus operations
{
   	NAND_CMDS= 0x20; 
//	Delay_325ns(1);
//  for(k=0;k<1;k++);
	var = NAND_DATA;
    NAND_CMDS= 0x22; 
}
/*Identify number of initial badblocks and store block status information
  into first 2 sectors of 32KB flash*/
void Check_Badblock(void)	
{
	u8 temp;
	u16 BBlockno;
	
	//Enable the Nand flash
	NAND_CMDS= 0x22;
	for(BBlockno=0;BBlockno<=MAXBLOCK;BBlockno++)
	{
	    //Put GPIO6 in output mode for writing data(default mode)
		GPIO6->DDR = 0xFF;
		SCU->GPIOOUT[6] = 0x5555;
		//Issue command for addressing spare area (last 16 bytes) of a page		
		NAND_DATA=0x50;			
		NF_Cmd();
		Nand_Address(0x05,0,BBlockno);
		do{
		temp = (NAND_CMDS&0x80);//GPIO_ReadBit(GPIO5,GPIO_Pin_7);	//check the ready/busy pin
		}while(!temp);						    //wait until it becomes high
		GPIO6->DDR = 0x00;					//Keep GPIO6 in input mode
		SCU->GPIOOUT[6] = 0x0000;
		NF_Read();
		if(var!=0xFF)
		{	
			DLEV2_FW_MCB_Store_block_status_info(BBlockno, BAD); 
		}
		else
		{
		  DLEV2_FW_MCB_Store_block_status_info(BBlockno, GOOD); 
		}	
	}
	//fBlock_Erase(MAXBLOCK);
	GPIO6->DDR = 0xFF;
	SCU->GPIOOUT[6] = 0x5555;		//Keep GPIO6 in output mode
	NAND_CMDS= 0x26;  //Disable the nandflash
}

u16 Select_Cmd(u16 Add)	  //it will select the command depends on byteno
{
	if(Add <= 255)
	NAND_DATA = 0x00;
	else if((Add > 255) && (Add < 512))
	{
	  NAND_DATA = 0x01;
      Add = Add - 256;
	}
	else if((Add > 512) && (Add < 528))
	{
	    NAND_DATA = 0x50;
		Add = Add - 512;
	}
	NF_Cmd();
	return Add;
}
u8 fBlock_Erase(u16 NBlock)				 
{
	u8 temp;

	GPIO6->DDR = 0xFF;
	SCU->GPIOOUT[6] = 0x5555;

	NAND_DATA=0x60;	//Block erase command
	NF_Cmd();
	NAND_DATA=(NBlock<<5);
	NF_Add();
	NAND_DATA=(NBlock>>3);
	NF_Add();
	NAND_DATA=(NBlock>>11);
	NF_Add();
	NAND_DATA=0xD0;	//confirm erase command
   	NF_Cmd();			
	do{
	temp =(NAND_CMDS&0x80); //GPIO_ReadBit(Pin_7);
	}while(!temp);
	NAND_DATA=0x70;
	NF_Cmd();
	GPIO6->DDR = 0x00;
	SCU->GPIOOUT[6] = 0x0000;
	NF_Read();
	GPIO6->DDR = 0xFF;
	SCU->GPIOOUT[6] = 0x5555;
	if(((var)&(0xC1)) == 0xC0)//if erase is completed return true
		return TRUE;
	else									
	{
		//if erase failed add to bad block list
		return FALSE;
	}
}

//Function to allocate first 16 good blocks for yard configuration data 
void Nand_Segments(void)
{
	u8 loop_var;
	u16 count=1;//u8
	u32 offset=4;
	for(loop_var = 0;loop_var<NO_OF_CONFIG_BLOCKS;)
	{
		if(*(u32*)(BADBLOCKLIST_BASE_ADDR+offset)==0xFFFFFFFF)
		{
			Config_Blocks[loop_var++] = count;
		}
		count++;
		offset = count*4;
	}
	
	/*Run loop till a good block is found for assigning
	  that block number to events storage start block*/
	while((*(u32*)(BADBLOCKLIST_BASE_ADDR + offset) == 0x00000000))
	{
		count++;
		offset = count*4;
	}

	Event_Block_Start = count;

	Nand_Start_Ptr.Blockno = Event_Block_Start;
	Nand_Start_Ptr.Page    = Verify_Badpage(Event_Block_Start, 0);
	Nand_Start_Ptr.Record  = 1;	




void Nand_Read_Records(Segment_Addr* Seg_Addr,u32 offset,u16 No_Of_Bytes)
{	
 	u16 NByte,NPage,NBlock,loop_var2,CByte;
	u8 temp,loop_var,index = 0;
	u32 offsetbyte;
	//u32 temp_page_no = 0;

	NByte  = Seg_Addr->Byte;
	NPage  = Seg_Addr->Page;
	NBlock = Seg_Addr->Block;

	offsetbyte = NByte+offset;

	for(loop_var = 0;loop_var<NO_OF_CONFIG_BLOCKS;loop_var++)
	{
		if(NBlock==Config_Blocks[loop_var])
		break;
	}

	while(offsetbyte>511)
	{
		offsetbyte = offsetbyte-512;
		NPage++;
		if(NPage>31)
		{
			NPage = 0;
			NBlock = Config_Blocks[++loop_var];
		}
	}

	NAND_CMDS= 0x22;
	GPIO6->DDR = 0xFF;
	SCU->GPIOOUT[6] = 0x5555;
	CByte = Select_Cmd(offsetbyte);
	Nand_Address(CByte,NPage,NBlock);
	do{
		temp = (NAND_CMDS&0x80);//GPIO_ReadBit(GPIO5,GPIO_Pin_7);
	}while(!temp);
	GPIO6->DDR = 0x00;
	SCU->GPIOOUT[6] = 0x0000;

	for(loop_var2=0;loop_var2<No_Of_Bytes;loop_var2++)
	{
		NF_Read();
		Tx_Data_Buffer[4+index]= var;
		index++;
		offsetbyte++;

		if(offsetbyte>511)
		{
			offsetbyte = 0;
			NPage++;
			if(NPage>31)
			{
				NPage = 0;
				for(loop_var= 0;loop_var<NO_OF_CONFIG_BLOCKS;loop_var++)
				{
					if(NBlock==Config_Blocks[loop_var])
						break;
				}
				NBlock = Config_Blocks[loop_var+1];
			}
			
			GPIO6->DDR = 0xFF;
			SCU->GPIOOUT[6] = 0x5555;
			offsetbyte = Select_Cmd(offsetbyte);
			Nand_Address(offsetbyte,NPage,NBlock);
			do{
			temp = (NAND_CMDS&0x80); 
			}while(!temp);
			GPIO6->DDR = 0x00;
			SCU->GPIOOUT[6] = 0x0000;
		}
	}
	GPIO6->DDR = 0xFF;
	SCU->GPIOOUT[6] = 0x5555;
	NAND_CMDS= 0x26;
}


u8 DLEV2_FW_MCB_Tx_ptr_backward_search(u8 port_no, u16 search_seq_no, u32 search_time)
{
	s8 temp_page_no;
	u8 temp_record_no;
	s16 temp_block_no;
	u16 temp_seq_no;
	u32 temp_ui_read_ptr;
	u32 temp_record_time;
	Data temp_record_ptr;
	u16 search_limit_block;

	if(port_no == 0x01)
	{
		temp_block_no  = Temp_NWPort1_Read_Ptr_Blockno;
		temp_page_no   = Temp_NWPort1_Read_Ptr_Page;   
		temp_record_no = Temp_NWPort1_Read_Ptr_Record;
		temp_ui_read_ptr = UI_NWPort1_Read_Ptr; 
	}
	else if(port_no == 0x02)
	{
		temp_block_no  = Temp_NWPort2_Read_Ptr_Blockno;
		temp_page_no   = Temp_NWPort2_Read_Ptr_Page;   
		temp_record_no = Temp_NWPort2_Read_Ptr_Record; 
		temp_ui_read_ptr = UI_NWPort2_Read_Ptr; 	
	}
	else if(port_no == 0x03)
	{
		temp_block_no  = LMU_Read_Ptr_Blockno;
		temp_page_no   = LMU_Read_Ptr_Page;
		temp_record_no = LMU_Read_Ptr_Record;
		temp_ui_read_ptr = UI_LMU_Read_Ptr;
	}
	else if(port_no == 0x05)
	{
		temp_block_no  = Temp_NWPort5_Read_Ptr_Blockno;
		temp_page_no   = Temp_NWPort5_Read_Ptr_Page;   
		temp_record_no = Temp_NWPort5_Read_Ptr_Record; 
		temp_ui_read_ptr = UI_NWPort5_Read_Ptr; 
	}
	else if(port_no == 0x06)
	{
		temp_block_no  = Temp_NWPort6_Read_Ptr_Blockno;
		temp_page_no   = Temp_NWPort6_Read_Ptr_Page;   
		temp_record_no = Temp_NWPort6_Read_Ptr_Record; 	
		temp_ui_read_ptr = UI_NWPort6_Read_Ptr; 
	}	
		
	if(temp_ui_read_ptr > 0)
	{
		temp_record_ptr = DLEV2_FW_MCB_Goto_previous_record(temp_block_no, temp_page_no, temp_record_no);
				
		temp_block_no  = temp_record_ptr.Blockno;                       
		temp_page_no   = temp_record_ptr.Page;
		temp_record_no = temp_record_ptr.Record;

		temp_record_time = DLEV2_FW_MCB_Read_record_time(temp_block_no, temp_page_no, temp_record_no); 
		if(search_time > temp_record_time)
		{
			return (INVALID_ENTRY);
		}

		temp_seq_no = DLEV2_FW_MCB_Read_record_seqno(temp_block_no, temp_page_no, temp_record_no);
		if(temp_seq_no > search_seq_no)
			temp_block_no = DLEV2_FW_MCB_Get_backward_search_block_no(temp_seq_no, search_seq_no, temp_block_no);
		else if(temp_seq_no < search_seq_no)
		{
			temp_block_no = DLEV2_FW_MCB_Get_backward_search_block_no(temp_seq_no, ZERO, temp_block_no);
			if(temp_block_no == SEARCH_FAIL)
				return(SEARCH_FAIL);
			temp_block_no = DLEV2_FW_MCB_Get_backward_search_block_no(MAX_SEQ_NO, search_seq_no, temp_block_no);
		}
		if(temp_block_no == SEARCH_FAIL)
			return(SEARCH_FAIL);
		temp_page_no = (search_seq_no % MAX_RECORDS_PER_BLOCK) / MAX_RECORDS_PER_PAGE;
		temp_record_no = 1;//(search_seq_no % MAX_RECORDS_PER_PAGE) + 1; 

		temp_seq_no = DLEV2_FW_MCB_Read_record_seqno(temp_block_no, temp_page_no, temp_record_no);
		while(temp_record_no < 32)
		{
			if(temp_seq_no == search_seq_no)
				break;
			
			temp_seq_no++;
			temp_record_no++;
		}
		temp_record_time = DLEV2_FW_MCB_Read_record_time(temp_block_no, temp_page_no, temp_record_no); 
		
		if((temp_seq_no == search_seq_no) && (temp_record_time == search_time))
		{
			switch(port_no)
			{
				case 0x01:
					NWPort1_Read_Ptr_Blockno = Temp_NWPort1_Read_Ptr_Blockno = temp_block_no;	
					NWPort1_Read_Ptr_Page    = Temp_NWPort1_Read_Ptr_Page    = temp_page_no;  
					NWPort1_Read_Ptr_Record  = Temp_NWPort1_Read_Ptr_Record  = temp_record_no;
					UI_NWPort1_Read_Ptr = Temp_UI_NWPort1_Read_Ptr = 
					DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);				
					break;
				
				case 0x02:
					NWPort2_Read_Ptr_Blockno = Temp_NWPort2_Read_Ptr_Blockno = temp_block_no;	
					NWPort2_Read_Ptr_Page    = Temp_NWPort2_Read_Ptr_Page    = temp_page_no;  
					NWPort2_Read_Ptr_Record  = Temp_NWPort2_Read_Ptr_Record  = temp_record_no;
					UI_NWPort2_Read_Ptr = Temp_UI_NWPort2_Read_Ptr = 
					DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);				
					break;

				case 0x03:
					LMU_Read_Ptr_Blockno = temp_block_no;
					LMU_Read_Ptr_Page    = temp_page_no;
					LMU_Read_Ptr_Record  = temp_record_no;
					UI_LMU_Read_Ptr = DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);	
					break;
						
				case 0x05:
					NWPort5_Read_Ptr_Blockno = Temp_NWPort5_Read_Ptr_Blockno = temp_block_no;	
					NWPort5_Read_Ptr_Page    = Temp_NWPort5_Read_Ptr_Page    = temp_page_no;  
					NWPort5_Read_Ptr_Record  = Temp_NWPort5_Read_Ptr_Record  = temp_record_no;
					UI_NWPort5_Read_Ptr = Temp_UI_NWPort5_Read_Ptr = 
					DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);				
					break;
				
				case 0x06:
					NWPort6_Read_Ptr_Blockno = Temp_NWPort6_Read_Ptr_Blockno = temp_block_no;	
					NWPort6_Read_Ptr_Page    = Temp_NWPort6_Read_Ptr_Page    = temp_page_no;  
					NWPort6_Read_Ptr_Record  = Temp_NWPort6_Read_Ptr_Record  = temp_record_no;
					UI_NWPort6_Read_Ptr = Temp_UI_NWPort6_Read_Ptr = 
					DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);				
					break;
			}
			return(SEARCH_SUCCESS);
		}
		else
		{
			if(common_mem_rollover_flag == 0)
				search_limit_block = Event_Block_Start;
			else
			{
				if(Write_rec_Ptr_Blockno > (MAXBLOCK - 2))
					search_limit_block = Event_Block_Start;
				else
					search_limit_block = Write_rec_Ptr_Blockno + 2;
			}
			while(temp_block_no >= search_limit_block)
			{
				temp_block_no = temp_block_no - 64;
				if(temp_block_no < Event_Block_Start)
				{
					if(common_mem_rollover_flag == 1)
						temp_block_no = (MAXBLOCK + 1) - (64 - (temp_block_no - Event_Block_Start));
					else
						return(SEARCH_FAIL);
				}
				if(temp_block_no >= search_limit_block)
				{
					temp_seq_no = DLEV2_FW_MCB_Read_record_seqno(temp_block_no, temp_page_no, temp_record_no);
					//If any bad blocks are present within last 64 blocks, the seq no doesn't match
					while(temp_seq_no != search_seq_no)
					{
						temp_block_no--;
						if(temp_block_no < search_limit_block)
							return(SEARCH_FAIL);
						else
							temp_block_no = DLEV2_FW_MCB_Get_good_block(temp_block_no);	
						
						temp_seq_no = DLEV2_FW_MCB_Read_record_seqno(temp_block_no, temp_page_no, temp_record_no);
					}
					temp_record_time = DLEV2_FW_MCB_Read_record_time(temp_block_no, temp_page_no, temp_record_no); 
					if(temp_record_time == search_time)
					{
						switch(port_no)
						{
							case 0x01:
								NWPort1_Read_Ptr_Blockno = Temp_NWPort1_Read_Ptr_Blockno = temp_block_no;	
								NWPort1_Read_Ptr_Page    = Temp_NWPort1_Read_Ptr_Page    = temp_page_no;  
								NWPort1_Read_Ptr_Record  = Temp_NWPort1_Read_Ptr_Record  = temp_record_no;
								UI_NWPort1_Read_Ptr = Temp_UI_NWPort1_Read_Ptr = 
								DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);				
								break;
							
							case 0x02:
								NWPort2_Read_Ptr_Blockno = Temp_NWPort2_Read_Ptr_Blockno = temp_block_no;	
								NWPort2_Read_Ptr_Page    = Temp_NWPort2_Read_Ptr_Page    = temp_page_no;  
								NWPort2_Read_Ptr_Record  = Temp_NWPort2_Read_Ptr_Record  = temp_record_no;
								UI_NWPort2_Read_Ptr = Temp_UI_NWPort2_Read_Ptr = 
								DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);				
								break;
					
							case 0x03:
								LMU_Read_Ptr_Blockno = temp_block_no;
								LMU_Read_Ptr_Page    = temp_page_no;
								LMU_Read_Ptr_Record  = temp_record_no;
								UI_LMU_Read_Ptr = DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);	
								break;

							case 0x05:
								NWPort5_Read_Ptr_Blockno = Temp_NWPort5_Read_Ptr_Blockno = temp_block_no;	
								NWPort5_Read_Ptr_Page    = Temp_NWPort5_Read_Ptr_Page    = temp_page_no;  
								NWPort5_Read_Ptr_Record  = Temp_NWPort5_Read_Ptr_Record  = temp_record_no;
								UI_NWPort5_Read_Ptr = Temp_UI_NWPort5_Read_Ptr = 
								DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);				
								break;
							
							case 0x06:
								NWPort6_Read_Ptr_Blockno = Temp_NWPort6_Read_Ptr_Blockno = temp_block_no;	
								NWPort6_Read_Ptr_Page    = Temp_NWPort6_Read_Ptr_Page    = temp_page_no;  
								NWPort6_Read_Ptr_Record  = Temp_NWPort6_Read_Ptr_Record  = temp_record_no;
								UI_NWPort6_Read_Ptr = Temp_UI_NWPort6_Read_Ptr = 
								DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);				
								break;
						}
						return(SEARCH_SUCCESS);
					}
					else
						continue;
				}
				else
					return(SEARCH_FAIL);
			}
			if(temp_block_no < search_limit_block)
				return(SEARCH_FAIL);
		}			
	}
	else
		return(SEARCH_FAIL);
}

u8 DLEV2_FW_MCB_Tx_ptr_forward_search(u8 port_no, u16 search_seq_no, u32 search_time)
{
	s8 temp_page_no;
	u8 temp_record_no;
	u16 temp_block_no;
	u16 temp_seq_no;
	u32 temp_ui_read_ptr;
	u32 temp_record_time;
	Data temp_record_ptr;
	u16 search_limit_block;
	u32 read_limit_pointer;
				
	if(port_no == 0x01)
	{
		temp_block_no  = Temp_NWPort1_Read_Ptr_Blockno;
		temp_page_no   = Temp_NWPort1_Read_Ptr_Page;   
		temp_record_no = Temp_NWPort1_Read_Ptr_Record;
		temp_ui_read_ptr = UI_NWPort1_Read_Ptr; 
	}
	else if(port_no == 0x02)
	{
		temp_block_no  = Temp_NWPort2_Read_Ptr_Blockno;
		temp_page_no   = Temp_NWPort2_Read_Ptr_Page;   
		temp_record_no = Temp_NWPort2_Read_Ptr_Record; 
		temp_ui_read_ptr = UI_NWPort2_Read_Ptr; 	
	}
	else if(port_no == 0x03)
	{
		temp_block_no  = LMU_Read_Ptr_Blockno;
		temp_page_no   = LMU_Read_Ptr_Page;
		temp_record_no = LMU_Read_Ptr_Record;
		temp_ui_read_ptr = UI_LMU_Read_Ptr;
	}
	else if(port_no == 0x05)
	{
		temp_block_no  = Temp_NWPort5_Read_Ptr_Blockno;
		temp_page_no   = Temp_NWPort5_Read_Ptr_Page;   
		temp_record_no = Temp_NWPort5_Read_Ptr_Record; 
		temp_ui_read_ptr = UI_NWPort5_Read_Ptr; 
	}
	else if(port_no == 0x06)
	{
		temp_block_no  = Temp_NWPort6_Read_Ptr_Blockno;
		temp_page_no   = Temp_NWPort6_Read_Ptr_Page;   
		temp_record_no = Temp_NWPort6_Read_Ptr_Record; 	
		temp_ui_read_ptr = UI_NWPort6_Read_Ptr; 
	}	
		
	//if(temp_ui_read_ptr > 0)
	{
		temp_record_ptr = DLEV2_FW_MCB_Goto_previous_record(temp_block_no, temp_page_no, temp_record_no);
				
		temp_block_no  = temp_record_ptr.Blockno;                       
		temp_page_no   = temp_record_ptr.Page;
		temp_record_no = temp_record_ptr.Record;

		temp_seq_no = DLEV2_FW_MCB_Read_record_seqno(temp_block_no, temp_page_no, temp_record_no);
	
		if(temp_seq_no < search_seq_no)
		{
			temp_block_no = DLEV2_FW_MCB_Get_forward_search_block_no(temp_seq_no, search_seq_no, temp_block_no);	
		}
		else if(temp_seq_no > search_seq_no)
		{
			temp_block_no = DLEV2_FW_MCB_Get_forward_search_block_no(temp_seq_no, MAX_SEQ_NO, temp_block_no);
			if(temp_block_no == SEARCH_FAIL)
				return(SEARCH_FAIL);
			temp_block_no = DLEV2_FW_MCB_Get_forward_search_block_no(ZERO, search_seq_no, temp_block_no);	
		}
		if(temp_block_no == SEARCH_FAIL)
				return(SEARCH_FAIL);
		temp_page_no = (search_seq_no % MAX_RECORDS_PER_BLOCK) / MAX_RECORDS_PER_PAGE;
		temp_record_no = 1;//(search_seq_no % MAX_RECORDS_PER_PAGE) + 1; 

		temp_seq_no = DLEV2_FW_MCB_Read_record_seqno(temp_block_no, temp_page_no, temp_record_no);
		while(temp_record_no < 32)
		{
			if(temp_seq_no == search_seq_no)
				break;
			
			temp_seq_no++;
			temp_record_no++;
		}
		temp_record_time = DLEV2_FW_MCB_Read_record_time(temp_block_no, temp_page_no, temp_record_no); 

		if((temp_seq_no == search_seq_no) && (temp_record_time == search_time))
		{
			temp_ui_read_ptr = DLEV2_FW_MCB_Get_ui_ptr_value(temp_block_no,temp_page_no,temp_record_no);
			 
			if(UI_Write_Ptr >= temp_ui_read_ptr)
				read_limit_pointer = UI_Write_Ptr;
			else if(UI_End_Cross_Write_Ptr >= temp_ui_read_ptr)
				read_limit_pointer = UI_End_Cross_Write_Ptr;

			if(temp_ui_read_ptr <= read_limit_pointer)
			{
				switch(port_no)
				{
					case 0x01:
						NWPort1_Read_Ptr_Blockno = Temp_NWPort1_Read_Ptr_Blockno = temp_block_no;	
						NWPort1_Read_Ptr_Page    = Temp_NWPort1_Read_Ptr_Page    = temp_page_no;  
						NWPort1_Read_Ptr_Record  = Temp_NWPort1_Read_Ptr_Record  = temp_record_no;
						UI_NWPort1_Read_Ptr = Temp_UI_NWPort1_Read_Ptr = temp_ui_read_ptr;
						break;
					
					case 0x02:
						NWPort2_Read_Ptr_Blockno = Temp_NWPort2_Read_Ptr_Blockno = temp_block_no;	
						NWPort2_Read_Ptr_Page    = Temp_NWPort2_Read_Ptr_Page    = temp_page_no;  
						NWPort2_Read_Ptr_Record  = Temp_NWPort2_Read_Ptr_Record  = temp_record_no;
						UI_NWPort2_Read_Ptr = Temp_UI_NWPort2_Read_Ptr = temp_ui_read_ptr;
						break;
			
					case 0x03:
						LMU_Read_Ptr_Blockno = temp_block_no;
						LMU_Read_Ptr_Page    = temp_page_no;
						LMU_Read_Ptr_Record  = temp_record_no;
						UI_LMU_Read_Ptr = temp_ui_read_ptr;
						break;
	
					case 0x05:
						NWPort5_Read_Ptr_Blockno = Temp_NWPort5_Read_Ptr_Blockno = temp_block_no;	
						NWPort5_Read_Ptr_Page    = Temp_NWPort5_Read_Ptr_Page    = temp_page_no;  
						NWPort5_Read_Ptr_Record  = Temp_NWPort5_Read_Ptr_Record  = temp_record_no;
						UI_NWPort5_Read_Ptr = Temp_UI_NWPort5_Read_Ptr = temp_ui_read_ptr;				
						break;
					
					case 0x06:
						NWPort6_Read_Ptr_Blockno = Temp_NWPort6_Read_Ptr_Blockno = temp_block_no;	
						NWPort6_Read_Ptr_Page    = Temp_NWPort6_Read_Ptr_Page    = temp_page_no;  
						NWPort6_Read_Ptr_Record  = Temp_NWPort6_Read_Ptr_Record  = temp_record_no;
						UI_NWPort6_Read_Ptr = Temp_UI_NWPort6_Read_Ptr = temp_ui_read_ptr;
						break;
				}
				return(SEARCH_SUCCESS);
			}
			else
				return(INVALID_ENTRY);
		}
		else
		{
			if(common_mem_rollover_flag == 0)
				search_limit_block = MAXBLOCK;
			else
			{
				if(Write_rec_Ptr_Blockno == Event_Block_Start)
					search_limit_block = MAXBLOCK; 
				else
					search_limit_block = Write_rec_Ptr_Blockno - 1;
			}
			
			while(temp_block_no <= search_limit_block)
			{
				temp_block_no = temp_block_no + 64;
				if(temp_block_no > MAXBLOCK)
				{
					if(common_mem_rollover_flag == 1)
						temp_block_no = (Event_Block_Start - 1) + (64 - (temp_block_no - MAXBLOCK));
					else
						return(SEARCH_FAIL);
				}
				
Data DLEV2_FW_MCB_Goto_previous_record(u16 temp_block_no, s8 temp_page_no, u8 temp_record_no)
{
	Data record_ptr;	

	temp_record_no--;
	//Take care of page and block changes when record number becomes zero
	if(temp_record_no == 0)
	{
		temp_record_no = 32;
		temp_page_no--;
		temp_page_no = DLEV2_FW_MCB_Get_good_page(temp_block_no, temp_page_no);
		//If the page 0 of current block is bad, move to last good page of below good block 
		if(temp_page_no < 0)
		{
			temp_block_no--;
			if(temp_block_no < Event_Block_Start)
			{
				if(common_mem_rollover_flag == 0)
					temp_block_no = Event_Block_Start;
				else
					temp_block_no = DLEV2_FW_MCB_Get_good_block(MAXBLOCK);	
			}
			else
				temp_block_no = DLEV2_FW_MCB_Get_good_block(temp_block_no);
			
			temp_page_no = DLEV2_FW_MCB_Get_good_page(temp_block_no, 31);
		}
	}	
	record_ptr.Blockno = temp_block_no;		
	record_ptr.Page    = temp_page_no;
	record_ptr.Record  = temp_record_no;

	return(record_ptr);
}


u16 DLEV2_FW_MCB_Get_backward_search_block_no(u32 temp_seq_no, u32 search_seq_no, u16 temp_block_no)
{
	u16 diff_step;
	u16 search_limit_block;

	if(common_mem_rollover_flag == 0)
		search_limit_block = Event_Block_Start;
	else
	{
		if(Write_rec_Ptr_Blockno > (MAXBLOCK - 2))
			search_limit_block = Event_Block_Start;
		else
			search_limit_block = Write_rec_Ptr_Blockno + 2;
	}

	diff_step = 1;
	while(temp_seq_no > search_seq_no)
	{
		if(((temp_seq_no % MAX_RECORDS_PER_PAGE) == 0) && ((temp_seq_no % MAX_RECORDS_PER_BLOCK) != 0))
		{
			diff_step = MAX_RECORDS_PER_PAGE;
		}
		else if((temp_seq_no % MAX_RECORDS_PER_BLOCK) == 0)
		{
			diff_step = MAX_RECORDS_PER_BLOCK;
			temp_block_no--; 
			if(temp_block_no < Event_Block_Start)
			{
				if(common_mem_rollover_flag == 0) 
					return(SEARCH_FAIL);
				else
					temp_block_no = DLEV2_FW_MCB_Get_good_block(MAXBLOCK);
			}
			else
				temp_block_no = DLEV2_FW_MCB_Get_good_block(temp_block_no);
			
			if(temp_block_no < search_limit_block)
				return(SEARCH_FAIL);
		}
		temp_seq_no -= diff_step;
	} 
	return(temp_block_no); 
}

u16 DLEV2_FW_MCB_Get_forward_search_block_no(u32 temp_seq_no, u32 search_seq_no, u16 temp_block_no)
{
	u16 diff_step;
	u16 search_limit_block;

	if(common_mem_rollover_flag == 0)
		search_limit_block = MAXBLOCK;
	else
	{
		if(Write_rec_Ptr_Blockno == Event_Block_Start)
			search_limit_block = MAXBLOCK; 
		else
			search_limit_block = Write_rec_Ptr_Blockno - 1;
	}

	diff_step = 1;
	while(temp_seq_no < search_seq_no)
	{
		temp_seq_no += diff_step;
		if(((temp_seq_no % MAX_RECORDS_PER_PAGE) == 0) && ((temp_seq_no % MAX_RECORDS_PER_BLOCK) != 0))
		{
			diff_step = MAX_RECORDS_PER_PAGE;
		}
		else if(((temp_seq_no % MAX_RECORDS_PER_BLOCK) == 0) && (temp_seq_no <= search_seq_no))
		{
			diff_step = MAX_RECORDS_PER_BLOCK;
			temp_block_no++; 
			if(temp_block_no > MAXBLOCK)
			{
				if(common_mem_rollover_flag == 0) 
					return(SEARCH_FAIL);
				else
					temp_block_no = Verify_BadBlock(Event_Block_Start);
			}
			else
				temp_block_no = Verify_BadBlock(temp_block_no);

			if(temp_block_no > search_limit_block)
				return(SEARCH_FAIL);
		}
	} 
	return(temp_block_no); 
}


